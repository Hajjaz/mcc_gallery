package gallery.hajjaz.com.gallery;

import android.content.Intent;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static android.R.id.list;

public class MainActivity extends AppCompatActivity {

    ArrayList<NameValuePair> postParameters;
    String url = "http://nationalappsbangladesh.com/mobsvc/ContentFile.php";
    public static ArrayList<String> image_array = new ArrayList<String>();
    ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new TheTask().execute();
    }

    class TheTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String str = null;
            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("AppleId", "9"));
            postParameters.add(new BasicNameValuePair("MenuId","35"));
            String response = null;
            try {
                response = CustomHttpClient.executeHttpPost(url, postParameters);

                String result = response.toString();
                System.out.println("Hajjaz response = "+result);
                return result;
            }catch (Exception e) {
                //Log.e("log_tag", "Error in http connection!!" + e.toString());

                //Toast.makeText(getActivity(),"Error Loading Data..", Toast.LENGTH_LONG).show();

            } finally{
            }

            return str;

        }



        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            String response = result.toString();
            try {

                JSONObject jsnobject = new JSONObject(response);
                JSONArray jsonArray = jsnobject.getJSONArray("contentfilelist");
                System.out.println("Hajjaz JSON = "+jsonArray);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject explrObject = jsonArray.getJSONObject(i);
                    image_array.add(explrObject.getString("IMG").toString());
                }

                final GridView gridview = (GridView) findViewById(R.id.gridview);
                gridview.setAdapter(new ImageAdapter(MainActivity.this, image_array));
                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        Intent i = new Intent(getApplicationContext(), FullImageActivity.class);
                        i.putExtra("id", position);
                        startActivity(i);
                    }
                });

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                // tv.setText("error2");
            }

        }
    }
}
